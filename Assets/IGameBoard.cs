﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets
{
    public interface IGameBoard
    {
        void Advance();
        void Reverse();
        void DragSelect(ICell firstSelectedCell, ICell finalSelectedCell);
        void ClearSelection();
        void Paste(BoardLocation boardLocation);
    }
}
