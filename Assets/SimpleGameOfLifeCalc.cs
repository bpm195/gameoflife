﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets
{
    public class SimpleGameOfLifeCalc : IGameOfLifeCalc
    {
        public int crowdMax=3;
        public int crowdMin=2;
        public bool areEdgesDead = true;
        private Stack<bool[,]> boardStack = new Stack<bool[,]>();
        private int width;
        private int height;

        private bool[,] newBoardState;

        public ICell[,] CalculateNextBoard(ICell[,] incomingBoard)
        {
            width = incomingBoard.GetLength(0);
            height = incomingBoard.GetLength(1);

            storeIncomingBoard(incomingBoard);
            newBoardState = new bool[incomingBoard.GetLength(0), incomingBoard.GetLength(1)];

            CalculateDelta(incomingBoard);
            return ApplyNewBoardState(incomingBoard);
        }

        private void storeIncomingBoard(ICell[,] incomingBoard)
        {

            var oldBoardDelta = new bool[width, height];
            //This shouldn't have to happen twice!
            for(int x=0; x<width; x++)
            {
                for(int y=0; y<height; y++)
                {
                    oldBoardDelta[x, y] = (incomingBoard[x, y].State() == CellState.Alive);
                }
            }
            boardStack.Push(oldBoardDelta);
        }

        public ICell[,] GoToPreviousBoard(ICell[,] incomingBoard)
        {
            if (boardStack.Count == 0)
            {
                return incomingBoard;
            }
            newBoardState = boardStack.Pop();
            return ApplyNewBoardState(incomingBoard);
        }

        private ICell[,] ApplyNewBoardState(ICell[,] board)
        {
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    if (newBoardState[x, y])
                    {
                        board[x, y].Birth();
                    }
                    else
                    {
                        board[x, y].Kill();
                    }
                }
            }
            return board;
        }

        private void CalculateDelta(ICell[,] board)
        {
            for (int x = 0; x < board.GetLength(0); x++)
            {
                for (int y = 0; y < board.GetLength(1); y++)
                {
                    var n = CountNeighbors(x, y, board);
                    newBoardState[x, y] = shouldCellLive(n, board[x, y]);
                }
            }
        }
//Standard Rules
//Any live cell with fewer than two live neighbours dies, as if caused by under-population.
//Any live cell with two or three live neighbours lives on to the next generation.
//Any live cell with more than three live neighbours dies, as if by over-population.
//Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
        public bool shouldCellLive(int neighbors, ICell cell)
        {
            if (cell.State() == CellState.Dead) { 
                return neighbors == 3;
            }
            if ((neighbors < crowdMin) || (neighbors > crowdMax))
            {
                return false;
            }
            return true;
        }

        public int CountNeighbors(int x, int y, ICell[,] board)
        {
            int count = 0;
            for (int p = x - 1; p <= x + 1; p++)
            {
                for (int q = y - 1; q <= y + 1; q++)
                {
                   
                    if ((p < 0) || (q < 0))
                    {
                        continue;
                    }
                    else if ((p >= board.GetLength(0) || (q >= board.GetLength(1))))
                    {
                        continue;
                    }
                    else if ((p == x) && (q == y))
                    {
                        continue;
                    }
                    else if (board[p, q].State() == CellState.Alive)
                    {
                        count++;
                    }
                }
            }
            return count;
        }
    }
}
