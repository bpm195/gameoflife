﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using NSubstitute;

namespace Assets
{
    public class SimpleGameCalcTests : GameOfLifeCalcTests
    {
        [Test]
        [TestCase(0, 0)]
        [TestCase(1, 1)]
        [TestCase(2, 2)]
        public void CountNeighbors_EmptyBoard_Zero(int x, int y)
        {
            int sizeX = 3;
            int sizeY = 3;

            var board = CreateEmptyBoard(sizeX, sizeY);
            var cut = new SimpleGameOfLifeCalc();

            var actual = cut.CountNeighbors(x, y, board);
            int expected = 0;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        [TestCase(0, 3)]
        [TestCase(4, 3)]
        [TestCase(3, 0)]
        [TestCase(3, 4)]
        public void CountNeighbors_CellOnEdge_NoNeighbors(int x, int y)
        {
            int size = 5;

            var loc = new BoardLocation(x, y);
            var board = CreatePopulatedBoard(size, size, new List<BoardLocation>() { loc });
            var cut = new SimpleGameOfLifeCalc();

            var actual = cut.CountNeighbors(x, y, board);
            int expected = 0;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        [TestCase(0, 0)]
        [TestCase(4, 0)]
        [TestCase(4, 4)]
        [TestCase(0, 4)]
        public void CountNeighbors_CellOnCorner_NoNeighbors(int x, int y)
        {
            int size = 5;

            var loc = new BoardLocation(x, y);
            var board = CreatePopulatedBoard(size, size, new List<BoardLocation>() { loc });
            var cut = new SimpleGameOfLifeCalc();

            var actual = cut.CountNeighbors(x, y, board);
            int expected = 0;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        [TestCase(2, 2)]
        [TestCase(2, 3)]
        [TestCase(2, 4)]
        [TestCase(3, 2)]
        [TestCase(3, 4)]
        [TestCase(4, 2)]
        [TestCase(4, 3)]
        [TestCase(4, 4)]
        public void CountNeighbors_OneNeighbor_ReturnsOne(int x, int y)
        {
            int size = 5;
            var controlCell = new BoardLocation(3, 3);

            var locations = new List<BoardLocation>
            {
                controlCell,
                new BoardLocation(x,y)
            };
            var board = CreatePopulatedBoard(size, size, locations);

            var cut = new SimpleGameOfLifeCalc();
            var actual = cut.CountNeighbors(3, 3, board);

            int expected = 1;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        [TestCase (0, false)]
        [TestCase (1, false)]
        [TestCase (2, true)]
        [TestCase (3, true)]
        [TestCase (4, false)]
        [TestCase (9, false)]
        public void ShouldCellLive_LivingCell_ReturnsSpecified(int neighbors, bool expected)
        {
            ICell mockCell = Substitute.For<ICell>();
            mockCell.State().Returns<CellState>(CellState.Alive);
            
            var cut = new SimpleGameOfLifeCalc();
            var actual = cut.shouldCellLive(neighbors, mockCell);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        [TestCase(0, false)]
        [TestCase(1, false)]
        [TestCase(2, false)]
        [TestCase(3, true)]
        [TestCase(4, false)]
        [TestCase(9, false)]
        public void ShouldCellLive_DeadCell_ReturnsSpecified(int neighbors, bool expected)
        {
            ICell mockCell = Substitute.For<ICell>();
            mockCell.State().Returns<CellState>(CellState.Dead);

            var cut = new SimpleGameOfLifeCalc();
            var actual = cut.shouldCellLive(neighbors, mockCell);

            Assert.AreEqual(expected, actual);
        }
    }
}
