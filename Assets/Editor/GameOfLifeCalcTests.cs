﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace Assets
{
    public class GameOfLifeCalcTests
    {
        protected ICell[,] CreateEmptyBoard(int rows, int columns)
        {
            if ((rows < 1) || (columns < 1))
            {
                throw new Exception("Board rows and columns must be positive ints");
            }
            var board = new ICell[rows, columns];

            for(int x = 0; x < rows; x++)
            {
                for(int y = 0;  y < columns; y++)
                {
                    board[x, y] = new Cell();
                }
            }

            return board;
        }

        protected ICell[,] CreatePopulatedBoard(int rows, int columns, List<BoardLocation> livingLocations)
        {
            var board = CreateEmptyBoard(rows, columns);
            foreach (BoardLocation loc in livingLocations)
            {
                board[loc.x, loc.y].Birth();
            }
            return board;
        }

        protected bool DoBoardsMatch(ICell[,] first, ICell[,] second)
        {
            if ((first.GetLength(0) != second.GetLength(0)) ||
                (first.GetLength(1)) != second.GetLength(1))
            {
                return false;
            }

            for (int x = 0; x < first.GetLength(0); x++) {
                for (int y = 0; y < first.GetLength(1); y++)
                {
                    if(first[x,y].State() != second[x, y].State())
                    {
                        var a = first[x, y];
                        var b = second[x, y];
                        return false;
                    }
                }
            }
            return true;
        }

        [Test]
        [TestCase(1, 1)]
        [TestCase(10, 10)]
        [TestCase(100, 100)]
        public void Calculate_EmptyBoardXxY_ReturnsSame(int sizeX, int sizeY)
        {
            var cut = new SimpleGameOfLifeCalc();

            var board = CreateEmptyBoard(sizeX, sizeY);
            var expected = CreateEmptyBoard(sizeX, sizeY);

            var actual = cut.CalculateNextBoard(board);

            Assert.That(DoBoardsMatch(expected, actual));
        }

        [Test]
        public void Calculate_SingleCell_Dies()
        {
            var rows = 10;
            var columns = 10;
            var setupLocations = new List<BoardLocation>() {
                new BoardLocation(1, 1)
                };
            var testBoard = CreatePopulatedBoard(rows, columns, setupLocations);

            var expected = CreateEmptyBoard(rows, columns);

            var cut = new SimpleGameOfLifeCalc();
            var actual = cut.CalculateNextBoard(testBoard);

            Assert.That(DoBoardsMatch(expected, actual));
        }
    }
}
