﻿//using UnityEngine; So long as these are commented out, we're happy about the design of this class.
//using UnityEditor;
using NUnit.Framework;

namespace Assets {
    //public class CellTests {
    //
    //    
    //    [Test]
    //    public void CellCreated_NoInitialState_IsDead()
    //    {
    //        ICell cut = new Cell();
    //
    //        var actual = cut.State();
    //        var expected = CellState.Dead;
    //
    //        Assert.AreEqual(expected, actual);
    //    }
    //
    //    [Test]
    //    [TestCase(CellState.Dead)]
    //    [TestCase(CellState.Alive)]
    //    public void CellCreatedWithInitialState_GainsInitialState(CellState value)
    //    {
    //        ICell cut = new Cell(value);
    //        var actual = cut.State();
    //
    //        Assert.AreEqual(value, actual);
    //    }
    //
    //    [Test]
    //    public void Kill_CellIsDead_RemainsDead()
    //    {
    //        ICell cut = new Cell();
    //
    //        cut.Kill();
    //
    //        var actual = cut.State();
    //        var expected = CellState.Dead;
    //
    //        Assert.AreEqual(expected, actual);
    //    }
    //
    //    [Test]
    //    public void Kill_CellIsAlive_IsDead()
    //    {
    //        ICell cut = new Cell(CellState.Alive);
    //        
    //        cut.Kill();
    //
    //        var actual = cut.State();
    //        var expected = CellState.Dead;
    //
    //        Assert.AreEqual(expected, actual);
    //
    //    }
    //
    //    [Test]
    //    public void Birth_CellIsDead_IsAlive()
    //    {
    //        ICell cut = new Cell();
    //
    //        cut.Birth();
    //
    //        var actual = cut.State();
    //        var expected = CellState.Alive;
    //
    //        Assert.AreEqual(expected, actual);
    //    }
    //
    //    [Test]
    //    public void Birth_CellIsAlive_IsAlive()
    //    {
    //
    //        ICell cut = new Cell(CellState.Alive);
    //        
    //        cut.Birth();
    //
    //        var actual = cut.State();
    //        var expected = CellState.Alive;
    //
    //        Assert.AreEqual(expected, actual);
    //    }
    //
    //    [Test]
    //    [TestCase(CellState.Alive)]
    //    [TestCase(CellState.Dead)]
    //    public void Equals_SameState_IsEqual(CellState state)
    //    {
    //        var first = new Cell(state);
    //        var second = new Cell(state);
    //
    //        Assert.AreEqual(first, second);
    //    }
    //
    //    [Test]
    //    [TestCase(CellState.Alive)]
    //    [TestCase(CellState.Dead)]
    //    public void  Equals_ComparedToNull_IsFalse(CellState state)
    //    {
    //        var cut = new Cell(state);
    //        Assert.AreNotEqual(cut, null);
    //    }
    //
    //    [Test]
    //    public void Equals_NotEqual_IsNotEqual()
    //    {
    //        var first  = new Cell(CellState.Alive);
    //        var second = new Cell(CellState.Dead);
    //
    //        Assert.AreNotEqual(first, second);
    //    }
    //}
}