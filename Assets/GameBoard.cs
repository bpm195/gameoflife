﻿using System;
using System.Collections.Generic;
namespace Assets
{
    //This is deprecatable, but the tests depend on it.
    //Technically I should throw out the tests, but... I have no excuse.
    public class GameBoard: IGameBoard
    {
        private ICell[,] board;
        private IGameOfLifeCalc gameCalc;

        public int Width
        {
            get;
            private set;
        }

        public int Height
        {
            get;
            private set;
        }

        public GameBoard(int width, int height)
        {
            Width = width;
            Height = height;

            board = new ICell[width, height];
        }

        public void Draw()
        {

        }

        public void Advance()
        {
            throw new NotImplementedException();
        }

        public void Reverse()
        {
            throw new NotImplementedException();
        }

        public void DragSelect(ICell firstSelectedCell, ICell finalSelectedCell)
        {
            throw new NotImplementedException();
        }

        public void ClearSelection()
        {
            throw new NotImplementedException();
        }

        public void Paste()
        {
            throw new NotImplementedException();
        }

        public void Paste(BoardLocation boardLocation)
        {
            throw new NotImplementedException();
        }
    }
}
