﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets
{
    public class BoardLocation
    {
        public int x;
        public int y;

        public BoardLocation(int _x, int _y)
        {
            x = _x;
            y = _y;
        }
    }
}
