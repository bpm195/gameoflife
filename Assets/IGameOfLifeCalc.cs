﻿using System.Collections.Generic;

namespace Assets
{
    public interface IGameOfLifeCalc
    {
        ICell[,] CalculateNextBoard(ICell[,] currentBoard);
        ICell[,] GoToPreviousBoard(ICell[,] currentBoard);
    }
}