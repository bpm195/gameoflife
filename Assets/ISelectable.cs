﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets
{
    public interface ISelectable: IClickable
    {
        void Select();
        void UnSelect();
    }
}
