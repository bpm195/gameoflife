﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets
{   //Deprecate this!
    public class Cell : ICell
    {
        public BoardLocation boardLocation { get; set; }
        public CellState initialState = CellState.Dead;
        private CellState state;
    
        public Cell(CellState _initialState)
        {
            state = _initialState;
        }
    
        public Cell()
        {
            state = initialState; 
        }
    
        public CellState State()
        {
            return state;
        }
    
        public void Kill()
        {
            state = CellState.Dead;
        }
    
        public void Birth()
        {
            state = CellState.Alive;
        }
    
        public override bool Equals(System.Object otherObject)
        {
            if (null == otherObject)
            {
                return false;
            }
            var otherCell = (otherObject as ICell);
    
            if (null == otherCell)
            {
                throw new InvalidCastException(
                    String.Format("Cannot compare {0} to {1}", this.GetType().ToString(), otherObject.GetType().ToString())
                );
            }
            return (otherCell.State() == this.State());
        }
        
    
        public override int GetHashCode()
        {
            //This should be agnostic to the content of this class.
            return base.GetHashCode(); 
        }

        public void Select()
        {
            throw new NotImplementedException();
        }

        public void UnSelect()
        {
            throw new NotImplementedException();
        }

        public void Click()
        {
            throw new NotImplementedException();
        }

        public void Set(CellState cellState)
        {
            throw new NotImplementedException();
        }
    }
}
