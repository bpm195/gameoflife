﻿using UnityEngine;
using System.Collections;
using System;

namespace Assets
{
    public class CellComponent : MonoBehaviour , ISelectable, ICell
    {
        public Material livingMaterial;
        public Material deadMaterial;
        public Material selectedMaterial;

        private Material previousMaterial;
        public BoardLocation boardLocation
        {
            get; set;
        } 
        private CellState state = CellState.Dead;
        private Renderer rend;


        public CellState State()
        {
            return state;
        }

        // Use this for initialization
        void Start()
        {
            rend = GetComponent<Renderer>();

            rend.material = deadMaterial;
        }

        // Update is called once per frame
        void Update()
        {
            
        }

        public void Set(CellState cellState)
        {
            if (cellState == CellState.Alive)
            {
                Birth();
            }
            if (cellState == CellState.Dead)
            {
                Kill();
            }
        }

        public void Kill()
        {
            state = CellState.Dead;
            rend.material = deadMaterial;
        }

        public void Birth()
        {
            state = CellState.Alive;
            rend.material = livingMaterial;
        }
        
        public void Click()
        {
            if (state == CellState.Alive)
            {
                Kill();
            }
            else if (state == CellState.Dead)
            {
                Birth();
            }
        }

        public void Select() {
            Debug.Log("Selectd!");
            previousMaterial = rend.material;
            rend.material = selectedMaterial;
        }
        public void UnSelect() {
            rend.material = previousMaterial;
        }

        
    }
}