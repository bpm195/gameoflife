﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace Assets
{
    public class GameBoardComponent : MonoBehaviour, IGameBoard
    {
        public int height = 4;
        public int width = 4;
        public CellComponent cellPrefab;
        public float scaleMultiplier = 1;
        public float cameraHeight = 10;
        public Canvas headsUpDisplay;
        private Camera camera;


        private ICell[,] board;
        private CellState[,] diff;
        private List<ICell> selectedCells = new List<ICell>();
        private List<ICell> copiedCells = new List<ICell>();
        private IGameOfLifeCalc gameCalc = new SimpleGameOfLifeCalc();

        // Use this for initialization
        void Start()
        {
            camera = Camera.main;
            board = new ICell[height, width];
            for (int x = 0; x < board.GetLength(0); x++)
            {
                for (int y = 0; y < board.GetLength(1); y++)
                {
                    float yOffset = ((float)board.GetLength(1)) / 2;
                    board[x, y] = (CellComponent)Instantiate(cellPrefab, new Vector3((x * scaleMultiplier), 0, (y * scaleMultiplier)), Quaternion.identity);
                    board[x, y].boardLocation = new BoardLocation(x, y);
                }
            }
            CenterCameraOverGameBoard();
        }

        // Update is called once per frame
        void Update()
        {
            UpdateHud();
        }

        void CenterCameraOverGameBoard()
        {
            Camera.main.transform.position = new Vector3(board.GetLength(0) * scaleMultiplier / 2, cameraHeight, board.GetLength(1) * scaleMultiplier / 2);
        }

        public void Advance()
        {
            Debug.Log("Advancing game");
            gameCalc.CalculateNextBoard(board);
        }

        public void Reverse()
        {
            Debug.Log("Reversing the game");
            gameCalc.GoToPreviousBoard(board);
        }

        public void UpdateHud()
        {

        }

        public void DragSelect(ICell firstSelectedCell, ICell finalSelectedCell)
        {
            int outerX = board.Length;
            int outerY = board.GetLength(1);

            BoardLocation minimumCorner =
                new BoardLocation(
                Math.Min(firstSelectedCell.boardLocation.x, finalSelectedCell.boardLocation.x),
                Math.Min(firstSelectedCell.boardLocation.y, finalSelectedCell.boardLocation.y)
                );

            BoardLocation maximumCorner =
                new BoardLocation(
                Math.Max(firstSelectedCell.boardLocation.x, finalSelectedCell.boardLocation.x),
                Math.Max(firstSelectedCell.boardLocation.y, finalSelectedCell.boardLocation.y)
                );

            int diffX = maximumCorner.x - minimumCorner.x;
            int diffY = maximumCorner.y - minimumCorner.y;
            diff = new CellState[diffX+1,diffY+1];

            for (int x = minimumCorner.x; x<=maximumCorner.x; x++)
            {
                for (int y = minimumCorner.y; y <= maximumCorner.y; y++)
                {
                    selectedCells.Add(board[x, y]);
                    board[x, y].Select();

                    int xDiff = x - minimumCorner.x;
                    int yDiff = y - minimumCorner.y;
                    Debug.Log(String.Format("xDiff {0}, yDiff {1}", xDiff, yDiff));
                    diff[xDiff, yDiff] = board[x, y].State();
                }
            }
        }

        public void ClearSelection()
        {
            foreach (ICell cell in selectedCells)
            {
                cell.UnSelect();
            }
            selectedCells.Clear();
        }

        public void Paste(BoardLocation boardLocation)
        {
            ClearSelection();
            int offSetX =  boardLocation.x;
            int offSetY = boardLocation.y;
            for(int x=0; x < diff.GetLength(0); x++)
            {
                for(int y=0; y<diff.GetLength(1); y++)
                {
                    board[x + offSetX, y + offSetY].Set(diff[x, y]);
                }
            }
        }

        
    }
}