﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace Assets
{
    public class PlayerInput : MonoBehaviour
    {
        private IGameBoard board;
        private bool selecting = false;
        private ICell firstSelectedCell;
        private ICell finalSelectedCell;



        // Use this for initialization
        void Start()
        {
            board = GetComponent<IGameBoard>();
        }

        // Update is called once per frame
        void Update()
        {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            bool hitSomething = Physics.Raycast(ray, out hit, 100);

            HighlightBehavior(hit, hitSomething);
            ClickBehavior(hit, hitSomething);
            ProgressBehavior(hit, hitSomething);
            CopyPasteBehavior(hit, hitSomething);
        }

        private void CopyPasteBehavior(RaycastHit hit, bool hitSomething)
        {
            if (Input.GetKeyDown(KeyCode.C))
            {
                //Copying is done by selecting.
            }
            if (Input.GetKeyDown(KeyCode.P))
            {
                if (!hitSomething)
                {
                    return;
                }
                var cell = hit.collider.GetComponent<ICell>() as ICell;
                if (null != cell)
                {
                    board.Paste(cell.boardLocation);
                }
            }
        }

        private void HighlightBehavior(RaycastHit hit, bool hitSomething)
        {
            if (Input.GetMouseButtonDown(1))
            {
                Debug.Log("Right Mouse down");
                Debug.Log("Clear Selection");
                board.ClearSelection();
                Debug.Log("Begin Selecting");
                selecting = true;
                if (hitSomething)
                {
                    var selectable = hit.collider.GetComponent<ISelectable>() as ISelectable;
                    if (null != selectable)
                    {
                        firstSelectedCell = hit.collider.GetComponent<ICell>() as ICell;
                    }
                }
            }
            else if (Input.GetMouseButtonUp(1))
            {
                Debug.Log("Right Mouse Up");
                Debug.Log("End Selecting");
                selecting = false;
                var selectable = hit.collider.GetComponent<ISelectable>() as ISelectable;
                if (null != selectable)
                {
                    //selectable.Select();
                    finalSelectedCell = hit.collider.GetComponent<ICell>() as ICell;
                    board.DragSelect(firstSelectedCell, finalSelectedCell);
                }
            }
        }
        private void ClickBehavior(RaycastHit hit, bool hitSomething)
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (hitSomething)
                {
                    var clickable = hit.collider.GetComponent<IClickable>() as IClickable;
                    if (null != clickable)
                    {
                        clickable.Click();
                    }
                }
            }
        }
        private void ProgressBehavior(RaycastHit hit, bool hitSomething)
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                board.Advance();
            }
            else if (Input.GetKeyDown(KeyCode.S))
            {
                board.Reverse();
            }
        }
    }
}
