﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets
{
    public interface ICell : ISelectable
    {
        CellState State();
        void Kill();
        void Birth();
        BoardLocation boardLocation { get; set; }
        void Set(CellState cellState);
    }
}
